package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api")
public class TaskController {


    @Autowired
    private TaskRepo repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompletableFuture<Task> create(@RequestBody Task task) {
        return repository.save(task).toFuture();
    }
}
