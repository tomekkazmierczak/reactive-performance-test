import java.util.UUID

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class PidRequestCreateSimulation extends Simulation {

  private def generateRandomId(): String = {
    UUID.randomUUID().toString
  }

  val scenarioName = "Create task"
  val httpProtocol = http.baseUrl("http://localhost:8080")
  val path = "/api"
  val jsonFile = "task.json"
  val requestPerSec = 2000
  val testDurationInSecond = 10

  val name = Iterator.continually(Map("name" -> generateRandomId()))

  val scn = scenario(scenarioName)
    .feed(name)
    .exec(http(scenarioName)
      .post(path)
      .body(RawFileBody(jsonFile)).asJson
      .check(status.is(201)))

  setUp(
    scn.inject(
      constantUsersPerSec(requestPerSec) during (testDurationInSecond)
    ).protocols(httpProtocol))
}